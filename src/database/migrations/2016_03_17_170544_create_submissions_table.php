<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');

            // Session type
            $table->integer('session_type_id')->unsigned()->nullable();
            $table->foreign('session_type_id')->references('id')->on('session_types')->onDelete('set null');

            // All the other data
            $table->longText('data');

            // Multi-tenancy
            $table->integer('conference_id')->unsigned()->nullable();
            $table->foreign('conference_id')->references('id')->on('conferences')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('submissions');
    }
}
