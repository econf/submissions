<?php

function subm_routes() {
    Route::group( [ 'namespace' => 'EConf\Submissions\Http\Controllers', 'middleware' => ['web', 'locale'] ], function () {

        Route::group(['middleware' => 'auth'], function(){
            Route::resource('submissions', 'SubmissionController');
            Route::get('submissions/{id}/details', 'SubmissionController@details');
            Route::post('submissions/{id}/details', 'SubmissionController@postDetails');
        });

        Route::group(['prefix' => 'admin', 'middleware' => 'committee:organizing'], function(){
            Route::resource('submission', 'AdminController', ['only' => ['index', 'show']]);
            Route::get( 'submissions', 'AdminController@index');
            Route::get( 'submissions/{id}', 'AdminController@show');
            Route::get( 'acceptance', 'AdminController@acceptance');
            Route::post( 'acceptance', 'AdminController@accept');
            Route::get( 'settings/submissions', 'SettingsController@show' );
            Route::post( 'settings/submissions', 'SettingsController@store' );
        });
    } );
}

if ( Config::get( 'econf.multi' ) ) {
    // Multi
    Route::group( [ 'prefix' => '{conf_slug}', 'middleware' => 'multi' ], function () {
        subm_routes();
    } );
} else {
    // Single
    subm_routes();
}

