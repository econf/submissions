<?php

namespace EConf\Submissions\Http\Controllers;

use App\Http\Controllers\Controller;
use App\SessionType;
use Illuminate\Http\Request;

use Setting;
use Flash;

class SettingsController extends Controller{

    public function show(){

        $types = SessionType::all();
        $current_types = explode(',', Setting::get('conf-subm-types', ''));

        return view('submissions::settings', compact('types', 'current_types'));
    }

    public function store(Request $request){
        $this->validate( $request, [
            'setting.conf-subm-start_date' => 'date_format:Y-m-d',
            'setting.conf-subm-end_date' => 'date_format:Y-m-d|after:setting.conf-subm-start_date',
            'setting.conf-subm-abstract_max_words' => 'integer|min:1'
        ], [], trans('submissions::submissions.attributes') );

        $setting = $request->setting;

        $setting['conf-subm-types'] = implode(',', array_keys(empty($request->types)?[]:$request->types) );

        $setting['conf-subm-author_fields'] = implode(',', array_keys(empty($request->author_fields)?[]:$request->author_fields));

        foreach (['allow_edit', 'keywords', 'abstract', 'document'] as $opt){
            $setting["conf-subm-{$opt}"] = $request->has($opt)?1:0;
        }

        foreach ( $setting as $key => $value ) {
            if ( trim( $value ) == false ) {
                Setting::forget( $key );
            } else {
                Setting::set( $key, trim( $value ) );
            }
        }

        Flash::success( trans( 'submissions::submissions.settings.success' ) );
        return redirect( m_action('\EConf\Submissions\Http\Controllers\SettingsController@show') );
    }

}
