<?php

namespace EConf\Submissions\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SessionType;
use EConf\Submissions\Submission;
use EConf\Submissions\SubmissionHelpers;
use Illuminate\Http\Request;

use Setting;
use Flash;
use Auth;
use Storage;
use File;
use Validator;

class SubmissionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        $submissions = Submission::where( 'user_id', Auth::id() )->paginate( 20 );
        return view( 'submissions::public.submissions.index', compact( 'submissions' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        if ( !SubmissionHelpers::isOpen() ) {
            abort( 403, 'Submissions are closed.' );
        }

        $types = SessionType::whereIn( 'id', explode( ',', Setting::get( 'conf-subm-types', '' ) ) )->get();

        $author_fields = [ ];

        SubmissionHelpers::authorFieldsEach( function ( $n, $f ) use ( &$author_fields ) {
            $author_fields[$n] = $f;
        } );

        return view( 'submissions::public.submissions.create', compact( 'types', 'author_fields' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {

        $this->validateWith( $this->getValidator( $request ) );

        $submission = new Submission();

        $submission->title = $request->title;
        $submission->session_type_id = $request->submission_type;

        $submission->user_id = Auth::id();

        $data = $request->all();
        unset( $data['_token'], $data['_method'], $data['topics'], $data['submission_type'] );

        $data['author'] = array_values( $data['author'] );

        if ( !empty( $data['keywords'] ) ) {
            $data['keywords'] = explode( PHP_EOL, $data['keywords'] );
            array_walk( $data['keywords'], function ( &$val, $idx ) {
                $val = trim( $val );
            } );
        }

        $submission->data = $data;

        $submission->save();

        $submission->topics()->sync( array_keys( $request->topics ) );

        // Here upload file if needed
        if ( $request->hasFile( 'document' ) && $request->file( 'document' )->isValid() ) {
            $file = $request->file( 'document' );
            $path = m_path( "submissions/{$submission->id}.pdf" );
            Storage::put( $path, File::get( $file ) );
        }

        Flash::success( trans( 'submissions::submissions.form.success' ) );

        return redirect( m_action( '\EConf\Submissions\Http\Controllers\SubmissionController@show', $submission ) );

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id ) {
        $submission = Submission::findByHiddenIdOrFail( $id );

        $this->authorize( $submission );

        $author_fields = [ ];

        SubmissionHelpers::authorFieldsEach( function ( $n, $f ) use ( &$author_fields ) {
            $author_fields[$n] = $f;
        } );

        return view( 'submissions::public.submissions.show', compact( 'submission', 'author_fields' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        $submission = Submission::findByHiddenIdOrFail( $id );

        $this->authorize( 'update', $submission );

        $types = SessionType::whereIn( 'id', explode( ',', Setting::get( 'conf-subm-types', '' ) ) )->get();

        $author_fields = [ ];

        SubmissionHelpers::authorFieldsEach( function ( $n, $f ) use ( &$author_fields ) {
            $author_fields[$n] = $f;
        } );

        return view( 'submissions::public.submissions.edit', compact( 'submission', 'types', 'author_fields' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {
        $submission = Submission::findByHiddenIdOrFail( $id );

        $this->authorize( $submission );

        $this->validateWith( $this->getValidator( $request, true ) );

        $submission->title = $request->title;
        $submission->session_type_id = $request->submission_type;

        $submission->user_id = Auth::id();

        $data = $request->all();
        unset( $data['_token'], $data['_method'], $data['topics'], $data['submission_type'] );

        $data['author'] = array_values( $data['author'] );

        $data['keywords'] = explode( PHP_EOL, $data['keywords'] );
        array_walk( $data['keywords'], function ( &$val, $idx ) {
            $val = trim( $val );
        } );

        $submission->data = $data;

        $submission->save();

        $submission->topics()->sync( array_keys( $request->topics ) );

        // Here upload file if needed
        if ( $request->hasFile( 'document' ) && $request->file( 'document' )->isValid() ) {
            $file = $request->file( 'document' );
            $path = m_path( "submissions/{$submission->id}.pdf" );
            Storage::put( $path, File::get( $file ) );
        }

        Flash::success( trans( 'submissions::submissions.form.edit_success' ) );

        return redirect( m_action( '\EConf\Submissions\Http\Controllers\SubmissionController@show', $submission ) );

    }

    public function details( $id ) {
        $submission = Submission::findByHiddenIdOrFail( $id );

        $this->authorize( $submission );

        return view( 'submissions::public.submissions.details', compact( 'submission' ) );
    }

    public function postDetails( Request $request, $id ) {
        $submission = Submission::findByHiddenIdOrFail( $id );

        $this->authorize( 'details', $submission );

        $new_data = $request->data;
        $new_data['final_details'] = true;

        // Here upload file if needed
        if ( $request->hasFile( 'final_document' ) && $request->file( 'final_document' )->isValid() ) {
            $file = $request->file( 'final_document' );
            $path = m_path( "submissions/final/{$submission->id}.pdf" );
            Storage::put( $path, File::get( $file ) );
        }

        $data = $submission->data;
        $data = array_merge( $new_data, $data );
        $submission->data = $data;
        $submission->save();

        Flash::success( trans('submissions::submissions.after_acceptance.save_successful'));

        return redirect( m_action( '\EConf\Submissions\Http\Controllers\SubmissionController@show', $submission ) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        $submission = Submission::findByHiddenIdOrFail( $id );

        $this->authorize( $submission );

        $submission->delete();

        Flash::success( trans( 'submissions::submissions.delete_submission_success' ) );
        return redirect( m_action( '\EConf\Submissions\Http\Controllers\SubmissionController@index' ) );
    }

    /**
     * @return array
     */
    private function generateValidationRules( $edit = false ) {
        $rules = [
            'title' => 'required',
            'topics' => 'required',
            'author' => 'required',
        ];

        if ( count( explode( ',', Setting::get( 'conf-subm-types', '' ) ) ) > 1 ) {
            $rules['submission_type'] = 'required|in:' . Setting::get( 'conf-subm-types' );
        }

        SubmissionHelpers::authorFieldsEach( function ( $n, $f ) use ( &$rules ) {
            $rules['author.*.' . $n] = $f['validation'];
        } );

        if ( Setting::get( 'conf-subm-keywords' ) ) {
            $rules['keywords'] = 'required';
        }
        if ( Setting::get( 'conf-subm-abstract' ) ) {
            $rules['abstract'] = 'required|words:' . Setting::get( 'conf-subm-abstract_max_words', 250 );
        }
        if ( Setting::get( 'conf-subm-document' ) ) {
            if ( $edit ) {
                $rules['document'] = 'mimes:pdf';
            } else {
                $rules['document'] = 'required|mimes:pdf';
            }
        }
        return $rules;
    }

    private function generateCustomAttributes( Request $request ) {

        $attributes = [ ];


        SubmissionHelpers::authorFieldsEach( function ( $n, $f ) use ( &$attributes ) {
            $attributes["author.*.{$n}"] = trans( "submissions::submissions.fields.author.{$n}" );
        } );

        return $attributes;

    }

    private function getValidator( Request $request, $edit = false ) {

        $validator = Validator::make( $request->all(),
            $this->generateValidationRules( $edit ),
            [ 'author.required' => trans( 'submissions::submissions.form.author_needed' ) ],
            $this->generateCustomAttributes( $request ) );

        $validator->after( function ( $validator ) use ( $request ) {

            $authors = collect( $request->author );

            $corresponding = $authors->filter( function ( $value, $key ) {
                return array_key_exists( 'corresponding', $value );
            } );

            if ( $corresponding->isEmpty() ) {
                $validator->errors()->add( 'author', trans( 'submissions::submissions.form.corresponding_needed' ) );
            }

        } );

        return $validator;
    }


}
