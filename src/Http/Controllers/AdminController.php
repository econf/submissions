<?php

namespace EConf\Submissions\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SessionType;
use EConf\Submissions\Submission;
use EConf\Submissions\SubmissionHelpers;
use Eventy;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Setting;
use Flash;
use Auth;
use Storage;
use File;

class AdminController extends Controller {
    const PER_PAGE = 20;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        // Add assets
        Eventy::addFilter( 'admin.assets', function ( $val ) {
            $val[] = 'bower_components/datatables.net/js/jquery.dataTables.min.js';
            $val[] = 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js';
            $val[] = 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css';

            $val[] = 'bower_components/datatables.net-responsive/js/dataTables.responsive.min.js';
            $val[] = 'bower_components/datatables.net-responsive-bs/js/responsive.bootstrap.js';
            $val[] = 'bower_components/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
            return $val;
        }, 10, 1 );

        $submissions = Submission::all();

        return view( 'submissions::submissions.index', compact( 'submissions' ) );
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id ) {
        $submission = Submission::findByHiddenIdOrFail($id);

        $author_fields = [ ];

        SubmissionHelpers::authorFieldsEach( function ( $n, $f ) use ( &$author_fields ) {
            $author_fields[$n] = $f;
        } );

        return view('submissions::submissions.show', compact('submission', 'author_fields'));
    }

    public function acceptance() {

        if(class_exists('EConf\Reviews\ReviewsServiceProvider')){
            return app('EConf\Reviews\Http\Controllers\AcceptanceController')->index();
        }

        // Add assets
        Eventy::addFilter( 'admin.assets', function ( $val ) {
            $val[] = 'bower_components/datatables.net/js/jquery.dataTables.min.js';
            $val[] = 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js';
            $val[] = 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css';

            $val[] = 'bower_components/datatables.net-responsive/js/dataTables.responsive.min.js';
            $val[] = 'bower_components/datatables.net-responsive-bs/js/responsive.bootstrap.js';
            $val[] = 'bower_components/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
            return $val;
        }, 10, 1 );

        $submissions = Submission::all();

        return view( 'submissions::submissions.acceptance', compact( 'submissions' ) );
    }

    public function accept(Request $request){

        $submissionsToAccept = array_keys($request->accept);

        $submissions = Submission::all();

        foreach ($submissions as $submission){
            $toAccept = in_array($submission->id, $submissionsToAccept);
            if($toAccept != $submission->accepted){
                $submission->accepted = $toAccept;
                $submission->save();
            }
        }

        Flash::success(trans('submissions::submissions.acceptance.save_successful'));
        return redirect( m_action('\EConf\Submissions\Http\Controllers\AdminController@acceptance'));

    }

}
