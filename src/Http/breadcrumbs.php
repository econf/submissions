<?php

// Settings > Submission
Breadcrumbs::register('admin.settings.submission', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.settings');
    $breadcrumbs->push(trans('submissions::submissions.settings.label_short'));
});

// Conference > Submissions
Breadcrumbs::register('admin.conference.submissions', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.conference');
    $breadcrumbs->push(trans('submissions::submissions.label'), m_action('\EConf\Submissions\Http\Controllers\AdminController@index'));
});

// Conference > Submission > Show
Breadcrumbs::register('admin.conference.submissions.show', function($breadcrumbs, $submission)
{
    $breadcrumbs->parent('admin.conference.submissions');
    $breadcrumbs->push($submission->title);
});

// Conference > Acceptance
Breadcrumbs::register('admin.conference.acceptance', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.conference');
    $breadcrumbs->push(trans('submissions::submissions.acceptance.label'), m_action('\EConf\Submissions\Http\Controllers\AdminController@acceptance'));
});
