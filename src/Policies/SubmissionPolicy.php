<?php

namespace EConf\Submissions\Policies;

use App\User;
use EConf\Submissions\Submission;
use EConf\Submissions\SubmissionHelpers;
use Illuminate\Auth\Access\HandlesAuthorization;

use Setting;

class SubmissionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, Submission $submission){
        return $user->id == $submission->user_id;
    }

    public function update(User $user, Submission $submission){
        if($user->id != $submission->user_id){
            return false;
        }

        if(SubmissionHelpers::isOpen()){
            return true;
        }

        if(Setting::get('conf-subm-allow_edit', false)){
            return true;
        }

        return false;
    }

    public function destroy(User $user, Submission $submission){
        return $this->update($user, $submission);
    }

    public function details(USer $user, Submission $submission){
        if($user->id != $submission->user_id){
            return false;
        }

        if(SubmissionHelpers::shouldAskFinalDetails() && $submission->accepted && !$submission->data('final_details', false)){
            return true;
        }

        return false;
    }
}
