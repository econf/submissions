<?php

namespace EConf\Submissions;

use App\SessionType;
use App\Topic;
use App\Traits\HasDataFieldTrait;
use App\Traits\HidesIds;
use App\Traits\TenantableTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Submission extends Model
{

    use TenantableTrait;

    use HidesIds;

    use HasDataFieldTrait;

    protected $casts = [
        'data' => 'array',
        'accepted' => 'boolean',
    ];

    public function topics(){
        return $this->belongsToMany(Topic::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function session_type(){
        return $this->belongsTo(SessionType::class);
    }

}
