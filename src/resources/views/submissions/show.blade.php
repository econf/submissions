@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ $submission->title }}
            @if($submission->accepted)
                <span class="label label-success">{{ trans('submissions::submissions.acceptance.accepted') }}</span>
            @endif
            <small>{{ $submission->session_type->name }}</small>
        </h1>
        {!! Breadcrumbs::render('admin.conference.submissions.show', $submission) !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')


        <h4>{{ trans('submissions::submissions.form.authors') }}</h4>

        <div class="row">

            @foreach($submission->data('author') as $author)
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <strong>{{ $author['name'] }}</strong>
                        </div>
                        <div class="box-body">

                            <dl class="dl-horizontal">

                                @foreach($author_fields as $name => $field)
                                    @if(!empty($author[$name]))
                                        <dt>
                                            {{ ucfirst(trans("submissions::submissions.fields.author.{$name}")) }}
                                            @if(!empty($field['icon']))
                                                <span class="text-primary fa {{$field['icon']}}"></span>
                                            @endif
                                        </dt>
                                        <dd>
                                            @if($field['type'] == "country")
                                                {{ Country::get($author[$name]) }}
                                            @elseif($field['type'] == "url")
                                                <a href="{{ $author[$name] }}">{{ $author[$name] }}</a>
                                            @elseif($field['type'] == "email")
                                                <a href="mailto:{{ $author[$name] }}">{{ $author[$name] }}</a>
                                            @elseif($field['type'] == "tel")
                                                <a href="tel:{{ $author[$name] }}">{{ phone_format($author[$name], $author['country']) }}</a>
                                            @elseif($field['type'] == "checkbox")
                                                {{ array_key_exists($name, $author)?trans('econf.misc.yes'):trans('econf.misc.no') }}
                                            @else
                                                {{ $author[$name] }}
                                            @endif
                                        </dd>

                                    @endif
                                @endforeach

                            </dl>

                        </div>
                    </div>
                </div>

            @endforeach

        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('submissions::submissions.form.content') }}
                </h3>
            </div>
            <div class="box-body">

                <p>
                    <strong>{{ trans('submissions::submissions.fields.topics') }}</strong><br>
                    {{ $submission->topics->implode('name', ', ') }}
                </p>

                @if(Setting::get('conf-subm-keywords', false))
                    <p>
                        <strong>{{ trans('submissions::submissions.fields.keywords') }}</strong><br>
                        {{ $submission->data('keywords')->implode(', ') }}
                    </p>
                @endif

                @if(Setting::get('conf-subm-abstract', false))
                    <p>
                        <strong>{{ trans('submissions::submissions.fields.abstract') }}</strong><br>
                        {{ $submission->data('abstract') }}
                    </p>
                @endif

                @if(Setting::get('conf-subm-document', false))
                    <p>
                        <strong>{{ trans('submissions::submissions.fields.document') }}</strong><br>
                        <a href="{{ action('FileController@get', m_path("submissions/{$submission->getRouteKey()}.pdf")) }}"
                           class="btn btn-default">
                            <span class="fa fa-download"></span>
                            {{ trans('submissions::submissions.download') }}
                        </a>
                    </p>
                @endif

            </div>
        </div>

    </section>


@endsection
