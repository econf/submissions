@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('submissions::submissions.label') }}
        </h1>
        {!! Breadcrumbs::render('admin.conference.submissions') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        <div class="box box-default">
            <div class="box-body">

                <table class="submissions-table table table-bordered table-striped dt-responsive" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>{{ trans('submissions::submissions.fields.id') }}</th>
                        <th data-priority="1">{{ trans('submissions::submissions.fields.title') }}</th>
                        <th>{{ trans('submissions::submissions.fields.keywords') }}</th>
                        <th data-priority="1">{{ trans('submissions::submissions.fields.submission_type') }}</th>
                        <th data-priority="2">{{ trans('submissions::submissions.form.authors') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($submissions as $submission)
                        <tr>
                            <td>{{ $submission->id }}</td>
                            <td>
                                <a href="{{ m_action('\EConf\Submissions\Http\Controllers\AdminController@show', $submission) }}">
                                    {{ $submission->title }}
                                </a>
                                @if($submission->accepted)
                                    <span class="text-success fa fa-check-circle"></span>
                                @endif
                            </td>
                            <td>{{ $submission->data('keywords')->implode(', ') }}</td>
                            <td>{{ $submission->session_type->name }}</td>
                            <td>{{ $submission->data('author')->implode('name', ', ') }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </section>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            var table = $('.submissions-table').DataTable(
                {!! json_encode(['language' => trans('econf.data_tables'), 'responsive' => ['details' => false]]) !!}
            );
        });
    </script>
@endsection
