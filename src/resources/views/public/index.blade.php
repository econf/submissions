@if(Setting::has('conf-subm-start_date') && Setting::has('conf-subm-end_date'))
    <h2>{{ trans('submissions::submissions.public.phase') }}</h2>
    <p>
        {{ Date::parse(Setting::get('conf-subm-start_date'))->format(trans('econf.date.mediumDate')) }}
        {{ trans('econf.public.to') }}
        {{ Date::parse(Setting::get('conf-subm-end_date'))->format(trans('econf.date.mediumDate')) }}
    </p>
    @if(\EConf\Submissions\SubmissionHelpers::isOpen())
        <a href="{{ m_action('\EConf\Submissions\Http\Controllers\SubmissionController@create') }}"
           class="btn btn-default">{{ trans('submissions::submissions.button_label') }}</a>
    @endif
@endif
