@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-file-text-o"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ trans('submissions::submissions.public.my') }}</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        @include('flash::message')

        <div class="panel panel-default">
            <table class="table">
                <thead>
                <tr>
                    <th>{{ trans('submissions::submissions.fields.title') }}</th>
                    @if(count( explode( ',', Setting::get( 'conf-subm-types', '' ) ) ) > 1)
                        <th class="hidden-xs">{{ trans('submissions::submissions.fields.submission_type') }}</th>
                    @endif
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($submissions as $submission)
                    <tr>
                        <td>
                            <a href="{{ m_action('\EConf\Submissions\Http\Controllers\SubmissionController@show', $submission) }}">
                                {{ $submission->title }}
                            </a>
                            @if(Setting::get('conf-subm-accpt-show', false) && $submission->accepted)
                                <span class="label label-success">{{ trans('submissions::submissions.acceptance.accepted') }}</span>
                            @endif
                        </td>
                        @if(count( explode( ',', Setting::get( 'conf-subm-types', '' ) ) ) > 1)
                            <td class="hidden-xs">{{ $submission->session_type->name }}</td>
                        @endif
                        <td class="text-right">
                            @can('update', $submission)
                                <a href="{{ m_action('\EConf\Submissions\Http\Controllers\SubmissionController@edit', $submission) }}"
                                   class="btn btn-default btn-xs" title="{{ trans('econf.actions.edit') }}">
                                    <span class="fa fa-pencil"></span>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="panel-footer">
                <div class="pull-right">
                    {!! $submissions->links() !!}
                </div>
            <span class="text-muted">
                {{ trans('econf.layout.results', ['from' => $submissions->firstItem(), 'to' => $submissions->lastItem(), 'total' => $submissions->total()]) }}
            </span>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@endsection
