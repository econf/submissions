@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-file-text-o"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ $submission->title }}</h1>
                    <p>{{ trans('submissions::submissions.after_acceptance.label') }}</p>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('\EConf\Submissions\Http\Controllers\SubmissionController@postDetails', $submission))->multipart() !!}

        @if(Setting::get('conf-subm-accpt-final_document', false))
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>{{ trans('submissions::submissions.after_acceptance.final_document') }}</strong>
                </div>
                <div class="panel-body">
                    <p class="helpBlock">{{ trans('submissions::submissions.after_acceptance.final_document_hint') }}</p>
                    {!! BootForm::file(trans('submissions::submissions.after_acceptance.final_document') , 'final_document')->helpBlock(trans('econf.layout.file_formats', ['types' => 'PDF']))->attribute('accept', 'application/pdf') !!}
                </div>
            </div>
        @endif

        @if(Setting::get('conf-subm-accpt-presenter', false))
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>{{ trans('submissions::submissions.after_acceptance.presenter') }}</strong>
                </div>
                <div class="panel-body">
                    @foreach($submission->data('author') as $idx => $author)
                        {!! BootForm::radio($author['name'], 'data[presenter]', $idx) !!}
                    @endforeach
                </div>
            </div>
        @endif

        <p>
            {!! BootForm::submit(trans('econf.actions.save'), 'btn-primary') !!}
        </p>

        {!! BootForm::close() !!}
    </div>
@endsection
