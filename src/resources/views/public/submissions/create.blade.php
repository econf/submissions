@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-file-text-o"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ trans('submissions::submissions.form.label') }}</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        @include('flash::message')

        <div>
            {!! Setting::get('conf-subm-instructions', '') !!}
        </div>

        {!! BootForm::open()->action(m_action('\EConf\Submissions\Http\Controllers\SubmissionController@store'))->multipart() !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ trans('submissions::submissions.form.general_info') }}
                </h3>
            </div>
            <div class="panel-body">
                {!! BootForm::text(trans('submissions::submissions.fields.title') . ' *', 'title', old('title')) !!}

                @if($types->count() > 1)
                    <div class="form-group {!! $errors->has('submission_type') ? 'has-error' : '' !!}">
                        <label
                            class="control-label">{{ trans('submissions::submissions.fields.submission_type') . ' *' }}</label>
                        <div>
                            @foreach($types as $type)
                                <label class="radio-inline">
                                    <input type="radio" name="submission_type" value="{{ $type->id }}"
                                           @if(old('submission_type') == $type->id) checked @endif>
                                    {{ $type->name }}
                                </label>
                            @endforeach
                        </div>
                        {!! $errors->first('submission_type', '<p class="help-block">:message</p>') !!}
                    </div>
                @elseif($types->count() == 1)
                    {!! BootForm::hidden('submission_type', $types->first()->id) !!}
                @endif
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ trans('submissions::submissions.form.author_info') }}
                </h3>
            </div>
            <div class="panel-body" id="authors">
                {!! $errors->first('author', '<div class="alert alert-danger">:message</div>') !!}
                @if(!empty(old('author')))
                    @foreach (array_keys(old('author')) as $idx)
                        <div class="panel panel-default" id="author{{ $idx }}">
                            <div class="panel-body">
                                @foreach($author_fields as $name => $af)
                                    @if($af['type'] == 'country')
                                        {!! Country::field(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author['.$idx.']['.$name.']', ucfirst(trans('submissions::submissions.fields.author.'.$name)), addslashes(old("author.{$idx}.{$name}", ''))) !!}
                                    @elseif($af['type'] == 'textarea')
                                        {!! BootForm::textarea(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author['.$idx.']['.$name.']')->rows(2)->value(addslashes(old("author.{$idx}.{$name}"))) !!}
                                    @elseif($af['type'] == 'checkbox')
                                        {!! BootForm::checkbox(ucfirst(trans('submissions::submissions.fields.author.'.$name)), 'author['.$idx.']['.$name.']')->setOldValue(old("author.{$idx}.{$name}")) !!}
                                    @else
                                        @if(!empty($af['icon']))
                                            {!! BootForm::inputGroup(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author['.$idx.']['.$name.']', addslashes(old("author.{$idx}.{$name}")))->type($af['type'])->beforeAddon("<span class=\"fa fa-fw {$af['icon']}\"></span>") !!}
                                        @else
                                            {!! BootForm::text(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author['.$idx.']['.$name.']', addslashes(old("author.{$idx}.{$name}")))->type($af['type']) !!}
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-xs btn-danger deleteAuthor" data-index="{{ $idx }}">
                                <span
                                    class="fa fa-trash"></span> {{ trans('submissions::submissions.form.remove_author') }}
                                </button>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="panel-footer">
                <a id="addAuthor" class="btn btn-sm btn-default">
                    <span class="fa fa-plus"></span> {{ trans('submissions::submissions.form.add_author') }}
                </a>
            </div>
        </div>

        <div class="panel {!! $errors->has('submission_type') ? 'panel-danger has-error' : 'panel-default' !!}">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ trans('submissions::submissions.fields.topics') }} *
                </h3>
            </div>
            <div class="panel-body">
                <p class="text-muted">{{ trans('submissions::submissions.form.topics_help') }}</p>
                @foreach(\App\Topic::orderBy('name')->get() as $topic)
                    {!! BootForm::checkbox($topic->name, "topics[{$topic->id}]")->setOldValue(old('topics.'.$topic->id), false) !!}
                @endforeach
                {!! $errors->first('topics', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        @if(Setting::get('conf-subm-keywords', false) or Setting::get('conf-subm-abstract', false) or Setting::get('conf-subm-document', false))

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        {{ trans('submissions::submissions.form.content') }}
                    </h3>
                </div>
                <div class="panel-body">

                    @if(Setting::get('conf-subm-keywords', false))
                        {!! BootForm::textarea(trans('submissions::submissions.fields.keywords') . ' *', 'keywords')->rows(2)->value(old('keywords'))->helpBlock(trans('submissions::submissions.form.keywords_help')) !!}
                    @endif

                    @if(Setting::get('conf-subm-abstract', false))
                        {!! BootForm::textarea(trans('submissions::submissions.fields.abstract') . ' *', 'abstract')->rows(2)->value(old('abstract'))->helpBlock(trans('submissions::submissions.form.abstract_help', ['words' => Setting::get('conf-subm-abstract_max_words', 250)])) !!}
                    @endif

                    @if(Setting::get('conf-subm-document', false))
                        {!! BootForm::file(trans('submissions::submissions.fields.document') , 'document')->helpBlock(trans('econf.layout.file_formats', ['types' => 'PDF']))->attribute('accept', 'application/pdf') !!}
                    @endif

                </div>
            </div>

        @endif

        <p>
            {!! BootForm::submit(trans('submissions::submissions.form.action'), 'btn-primary') !!}
        </p>

        {!! BootForm::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        function template(idx) {
            var tpl = '<div class="panel panel-default" id="author' + idx + '">' +
                      '<div class="panel-body">' +
                      @foreach($author_fields as $name => $af)
                          @if($af['type'] == 'country')
                          '{!! Country::field(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author[\'+idx+\']['.$name.']', ucfirst(trans('submissions::submissions.fields.author.'.$name)), '', true) !!}' +
                      @elseif($af['type'] == 'textarea')
                          '{!! BootForm::textarea(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author[\'+idx+\']['.$name.']')->rows(2) !!}' +
                      @elseif($af['type'] == 'checkbox')
                          '{!! BootForm::checkbox(ucfirst(trans('submissions::submissions.fields.author.'.$name)), 'author[\'+idx+\']['.$name.']') !!}' +
                      @else
                          @if(!empty($af['icon']))
                          '{!! BootForm::inputGroup(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author[\'+idx+\']['.$name.']')->type($af['type'])->beforeAddon("<span class=\"fa fa-fw {$af['icon']}\"></span>") !!}' +
                      @else
                          '{!! BootForm::text(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author[\'+idx+\']['.$name.']')->type($af['type']) !!}' +
                      @endif
                          @endif
                          @endforeach
                          '</div>' +
                      '<div class="panel-footer">' +
                      '<button class="btn btn-xs btn-danger deleteAuthor" data-index="' + idx + '">' +
                      '<span class="fa fa-trash"></span> {{ trans('submissions::submissions.form.remove_author') }}' +
                      '</button>' +
                      '</div>' +
                      '</div>';
            return tpl;
        }

        var idx = {{ !empty(old('author'))?Illuminate\Support\Arr::last(array_keys(old('author')))+1:0 }};

        $(document).ready(function () {
            $("#addAuthor").click(function () {
                $("#authors").append(template(idx));
                $('#author' + idx).find('select').select2({
                    theme: 'bootstrap'
                });
                idx++;
            });

            if ($('#authors').children().length == 0) {
                $("#addAuthor").click();
            }

            $('#authors').on('click', '.deleteAuthor', function (e) {
                e.preventDefault();
                console.log('test');
                var index = $(this).attr('data-index');
                console.log(index);
                $('#author' + index).remove();
            });
        });
    </script>
@endsection
