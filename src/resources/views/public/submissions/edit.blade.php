@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-file-text-o"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ trans('submissions::submissions.form.label') }}</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="page-header">
            <h1>{{ trans('submissions::submissions.form.label') }}</h1>
        </div>

        @include('flash::message')

        <div>
            {!! Setting::get('conf-subm-instructions', '') !!}
        </div>

        {!! BootForm::open()->action(m_action('\EConf\Submissions\Http\Controllers\SubmissionController@update', $submission))->multipart()->put() !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ trans('submissions::submissions.form.general_info') }}
                </h3>
            </div>
            <div class="panel-body">
                {!! BootForm::text(trans('submissions::submissions.fields.title') . ' *', 'title', old('title', $submission->title)) !!}

                @if($types->count() > 1)
                    <div class="form-group {!! $errors->has('submission_type') ? 'has-error' : '' !!}">
                        <label
                            class="control-label">{{ trans('submissions::submissions.fields.submission_type') . ' *' }}</label>
                        <div>
                            @foreach($types as $type)
                                <label class="radio-inline">
                                    <input type="radio" name="submission_type" value="{{ $type->id }}"
                                           @if(old('submission_type', $submission->session_type_id) == $type->id) checked @endif>
                                    {{ $type->name }}
                                </label>
                            @endforeach
                        </div>
                        {!! $errors->first('submission_type', '<p class="help-block">:message</p>') !!}
                    </div>
                @elseif($types->count() == 1)
                    {!! BootForm::hidden('submission_type', $types->first()->id) !!}
                @endif
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ trans('submissions::submissions.form.author_info') }}
                </h3>
            </div>
            <div class="panel-body" id="authors">
                {!! $errors->first('author', '<div class="alert alert-danger">:message</div>') !!}
                @if(!empty(old('author', $submission->data['author'])))
                    @foreach (array_keys(old('author', $submission->data['author'])) as $idx)
                        <div class="panel panel-default" id="author{{ $idx }}">
                            <div class="panel-body">
                                @foreach($author_fields as $name => $af)
                                    @if($af['type'] == 'country')
                                        {!! Country::field(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author['.$idx.']['.$name.']', ucfirst(trans('submissions::submissions.fields.author.'.$name)), old("author.{$idx}.{$name}", $submission->data("author.{$idx}.{$name}", ""))) !!}
                                    @elseif($af['type'] == 'textarea')
                                        {!! BootForm::textarea(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author['.$idx.']['.$name.']')->rows(2)->value(old("author.{$idx}.{$name}", $submission->data("author.{$idx}.{$name}"))) !!}
                                    @elseif($af['type'] == 'checkbox')
                                        {!! BootForm::checkbox(ucfirst(trans('submissions::submissions.fields.author.'.$name)), 'author['.$idx.']['.$name.']')->setOldValue(old("author.{$idx}.{$name}", $submission->data("author.{$idx}.{$name}"))) !!}
                                    @else
                                        @if(!empty($af['icon']))
                                            {!! BootForm::inputGroup(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author['.$idx.']['.$name.']', old("author.{$idx}.{$name}", $submission->data("author.{$idx}.{$name}")))->type($af['type'])->beforeAddon("<span class=\"fa fa-fw {$af['icon']}\"></span>") !!}
                                        @else
                                            {!! BootForm::text(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author['.$idx.']['.$name.']', old("author.{$idx}.{$name}", $submission->data("author.{$idx}.{$name}")))->type($af['type']) !!}
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-xs btn-danger deleteAuthor" data-index="{{ $idx }}">
                                <span
                                    class="fa fa-trash"></span> {{ trans('submissions::submissions.form.remove_author') }}
                                </button>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="panel-footer">
                <a id="addAuthor" class="btn btn-sm btn-default">
                    <span class="fa fa-plus"></span> {{ trans('submissions::submissions.form.add_author') }}
                </a>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ trans('submissions::submissions.fields.topics') }} *
                </h3>
            </div>
            <div class="panel-body">
                <p class="text-muted">{{ trans('submissions::submissions.form.topics_help') }}</p>
                @foreach(\App\Topic::orderBy('name')->get() as $topic)
                    {!! BootForm::checkbox($topic->name, "topics[{$topic->id}]")->setOldValue(old('topics.'.$topic->id, $submission->topics->contains('id', $topic->id)), false) !!}
                @endforeach
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ trans('submissions::submissions.form.content') }}
                </h3>
            </div>
            <div class="panel-body">

                @if(Setting::get('conf-subm-keywords', false))
                    {!! BootForm::textarea(trans('submissions::submissions.fields.keywords') . ' *', 'keywords')->rows(2)->value(old('keywords', $submission->data('keywords', [])->implode(PHP_EOL)))->helpBlock(trans('submissions::submissions.form.keywords_help')) !!}
                @endif

                @if(Setting::get('conf-subm-abstract', false))
                    {!! BootForm::textarea(trans('submissions::submissions.fields.abstract') . ' *', 'abstract')->rows(2)->value(old('abstract', $submission->data('abstract')))->helpBlock(trans('submissions::submissions.form.abstract_help', ['words' => Setting::get('conf-subm-abstract_max_words', 250)])) !!}
                @endif

                @if(Setting::get('conf-subm-document', false))
                    {!! BootForm::file(trans('submissions::submissions.fields.document') , 'document')->helpBlock(trans('submissions::submissions.form.keep_empty').' '.trans('econf.layout.file_formats', ['types' => 'PDF']))->attribute('accept', 'application/pdf') !!}
                @endif

            </div>
        </div>

        <div class="pull-right">
            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                <span class="fa fa-trash"></span>
                {{ trans('econf.actions.delete') }}
            </a>
        </div>

        <p>
            {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}
        </p>

        {!! BootForm::close() !!}

        <div class="modal modal-danger" id="deleteModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">{{ trans('submissions::submissions.delete_submission') }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>{!! trans('submissions::submissions.delete_submission_confirmation', ['name' => $submission->title]) !!}</p>
                        <p>{!! trans('econf.layout.delete_warning') !!}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            {{ trans('econf.actions.cancel') }}
                        </button>
                        {!! BootForm::open()->action(m_action('\EConf\Submissions\Http\Controllers\SubmissionController@destroy', $submission))->delete() !!}
                        {!! BootForm::submit(trans('econf.actions.delete'), 'btn-danger') !!}
                        {!! BootForm::close() !!}
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function template(idx) {
            var tpl = '<div class="panel panel-default" id="author' + idx + '">' +
                      '<div class="panel-body">' +
                      @foreach($author_fields as $name => $af)
                          @if($af['type'] == 'country')
                          '{!! Country::field(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author[\'+idx+\']['.$name.']', ucfirst(trans('submissions::submissions.fields.author.'.$name)), '', true) !!}' +
                      @elseif($af['type'] == 'textarea')
                          '{!! BootForm::textarea(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author[\'+idx+\']['.$name.']')->rows(2) !!}' +
                      @elseif($af['type'] == 'checkbox')
                          '{!! BootForm::checkbox(ucfirst(trans('submissions::submissions.fields.author.'.$name)), 'author[\'+idx+\']['.$name.']') !!}' +
                      @else
                          @if(!empty($af['icon']))
                          '{!! BootForm::inputGroup(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author[\'+idx+\']['.$name.']')->type($af['type'])->beforeAddon("<span class=\"fa fa-fw {$af['icon']}\"></span>") !!}' +
                      @else
                          '{!! BootForm::text(ucfirst(trans('submissions::submissions.fields.author.'.$name)) . (empty($af['mandatory'])?'':' *'), 'author[\'+idx+\']['.$name.']')->type($af['type']) !!}' +
                      @endif
                          @endif
                          @endforeach
                          '</div>' +
                      '<div class="panel-footer">' +
                      '<button class="btn btn-xs btn-danger deleteAuthor" data-index="' + idx + '">' +
                      '<span class="fa fa-trash"></span> {{ trans('submissions::submissions.form.remove_author') }}' +
                      '</button>' +
                      '</div>' +
                      '</div>';
            return tpl;
        }

        var idx = {{ !empty(old('author', $submission->data['author']))?Illuminate\Support\Arr::last(array_keys(old('author', $submission->data['author'])))+1:0 }};

        $(document).ready(function () {
            $("#addAuthor").click(function () {
                $("#authors").append(template(idx));
                $('#author' + idx).find('select').select2({
                    theme: 'bootstrap'
                });
                idx++;
            });

            if ($('#authors').children().length == 0) {
                $("#addAuthor").click();
            }

            $('#authors').on('click', '.deleteAuthor', function (e) {
                e.preventDefault();
                console.log('test');
                var index = $(this).attr('data-index');
                console.log(index);
                $('#author' + index).remove();
            });
        });
    </script>
@endsection
