@extends('layouts.public')

@section('content')

    <section id="page-breadcrumb">
        <div class="icon fa fa-file-text-o"></div>
        <div class="vertical-center">
            <div class="container">
                <div class="action">
                    <h1 class="title">{{ $submission->title }}</h1>
                    @if(count( explode( ',', Setting::get( 'conf-subm-types', '' ) ) ) > 1)
                        <p>{{ $submission->session_type->name }}</p>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        @if(Setting::get('conf-subm-accpt-show', false) && $submission->accepted)
            <div class="alert alert-success">
                <strong>{{ trans('submissions::submissions.acceptance.accepted_callout_title', ['name' => Setting::get('conf-short_name', Setting::get('conf-name'))]) }}</strong>
                @if(\EConf\Submissions\SubmissionHelpers::shouldAskFinalDetails() && !$submission->data('final_details', false))
                    {{ trans('submissions::submissions.acceptance.accepted_callout_message') }}
                @endif
            </div>
        @endif

        @include('flash::message')

        <h3>{{ trans('submissions::submissions.form.authors') }}</h3>

        <div class="row">

            @foreach($submission->data('author') as $idx => $author)
                <div class="col-md-6">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>{{ $author['name'] }}</strong>
                            @if(!is_null($submission->data('presenter')) && $submission->data('presenter') == $idx)
                                <span
                                    class="label label-info">{{ trans('submissions::submissions.after_acceptance.presenter') }}</span>
                            @endif
                        </div>
                        <div class="panel-body">

                            @foreach($author_fields as $name => $field)
                                @if(!empty($author[$name]))
                                    <div class="row">
                                        <strong class="col-sm-4">
                                            {{ ucfirst(trans("submissions::submissions.fields.author.{$name}")) }}
                                            @if(!empty($field['icon']))
                                                <span class="text-primary fa {{$field['icon']}}"></span>
                                            @endif
                                        </strong>
                                        <p class="col-sm-8">
                                            @if($field['type'] == "country")
                                                {{ Country::get($author[$name]) }}
                                            @elseif($field['type'] == "url")
                                                <a href="{{ $author[$name] }}">{{ $author[$name] }}</a>
                                            @elseif($field['type'] == "email")
                                                <a href="mailto:{{ $author[$name] }}">{{ $author[$name] }}</a>
                                            @elseif($field['type'] == "tel")
                                                <a href="tel:{{ $author[$name] }}">{{ phone_format($author[$name], $author['country']) }}</a>
                                            @elseif($field['type'] == "checkbox")
                                                {{ array_key_exists($name, $author)?trans('econf.misc.yes'):trans('econf.misc.no') }}
                                            @else
                                                {{ $author[$name] }}
                                            @endif
                                        </p>
                                    </div>
                                @endif
                            @endforeach

                        </div>
                    </div>

                </div>
            @endforeach

        </div>

        <h3>{{ trans('submissions::submissions.form.content') }}</h3>

        <div class="row">

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>{{ trans('submissions::submissions.fields.topics') }}</strong>
                    </div>
                    <ul class="list-group">
                        @foreach($submission->topics as $topic)
                            <li class="list-group-item">{{ $topic->name }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

            @if(Setting::get('conf-subm-keywords', false))
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>{{ trans('submissions::submissions.fields.keywords') }}</strong>
                        </div>
                        <ul class="list-group">
                            @foreach($submission->data['keywords'] as $keyword)
                                <li class="list-group-item">{{ $keyword }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            @if(Setting::get('conf-subm-abstract', false))
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>{{ trans('submissions::submissions.fields.abstract') }}</strong>
                        </div>
                        <div class="panel-body">
                            {{ $submission->data['abstract'] }}
                        </div>
                    </div>
                </div>
            @endif

            @if(Setting::get('conf-subm-document', false) && Storage::exists(m_path("submissions/{$submission->id}.pdf")))
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>{{ trans('submissions::submissions.fields.document') }}</strong>
                        </div>
                        <div class="panel-body">
                            <a href="{{ action('FileController@get', m_path("submissions/{$submission->getRouteKey()}.pdf")) }}"
                               class="btn btn-default">
                                <span class="fa fa-download"></span>
                                {{ trans('submissions::submissions.download') }}
                            </a>
                            @if(Storage::exists(m_path("submissions/final/{$submission->id}.pdf")))
                                <a href="{{ action('FileController@get', m_path("submissions/final/{$submission->getRouteKey()}.pdf")) }}"
                                   class="btn btn-default">
                                    <span class="fa fa-download"></span>
                                    {{ trans('submissions::submissions.after_acceptance.final_document') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        </div>

        @can('update', $submission)
            <a href="{{ m_action('\EConf\Submissions\Http\Controllers\SubmissionController@edit', $submission) }}"
               class="btn btn-default">
                <span class="fa fa-pencil"></span>
                {{ trans('econf.actions.edit') }}
            </a>
        @endcan

        @can('details', $submission)
            <a href="{{ m_action('\EConf\Submissions\Http\Controllers\SubmissionController@details', $submission) }}"
               class="btn btn-default">
                {{ trans('submissions::submissions.after_acceptance.submit') }}
            </a>
        @endcan

    </div>
@endsection
