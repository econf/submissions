@extends('layouts.admin')

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('submissions::submissions.settings.label') }}
        </h1>
        {!! Breadcrumbs::render('admin.settings.submission') !!}
    </section>

    <!-- Main content -->
    <section class="content">

        @include('flash::message')

        {!! BootForm::open()->action(m_action('\EConf\Submissions\Http\Controllers\SettingsController@store'))->post() !!}

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('submissions::submissions.settings.phase_settings') }}
                </h3>
            </div>
            <div class="box-body">

                {!! setting_input(trans('submissions::submissions.settings.start_date'), 'conf-subm-start_date', 'date') !!}
                {!! setting_input(trans('submissions::submissions.settings.end_date'), 'conf-subm-end_date', 'date') !!}

                <div class="form-group">
                    <label>{{ trans('submissions::submissions.settings.session_types') }}</label>
                    <div>
                        @foreach($types as $type)
                            <label class="checkbox-inline">
                                <input type="checkbox" name="types[{{ $type->id }}]" value="yes"
                                       @if(in_array($type->id, $current_types)) checked @endif>
                                {{ $type->name }}
                            </label>
                        @endforeach
                    </div>

                </div>

                {!! BootForm::checkbox(trans('submissions::submissions.settings.allow_edit'), 'allow_edit')->setOldValue(Setting::get('conf-subm-allow_edit', false)) !!}

            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ trans('submissions::submissions.settings.form_settings') }}
                </h3>
            </div>
            <div class="box-body">

                <div class="form-group">
                    <label class="control-label">{{ trans('submissions::submissions.settings.author_info_fields') }}</label>

                    @foreach(\EConf\Submissions\SubmissionHelpers::$fields as $name => $field)

                        @if(empty($field['mandatory']))
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="author_fields[{{ $name }}]" value="yes"
                                           @if(in_array($name, explode(',', Setting::get('conf-subm-author_fields', '')))) checked @endif>
                                    {{ ucfirst(trans('submissions::submissions.fields.author.'.$name)) }}
                                </label>
                            </div>
                        @else
                            <div class="checkbox disabled">
                                <label>
                                    <input type="checkbox" checked disabled>
                                    {{ ucfirst(trans('submissions::submissions.fields.author.'.$name)) }}
                                </label>
                            </div>
                        @endif

                    @endforeach

                </div>

                <div class="form-group">
                    <label class="control-label">{{ trans('submissions::submissions.form.content') }}</label>

                    {!! BootForm::checkbox(trans('submissions::submissions.fields.keywords'), 'keywords')->defaultCheckedState(Setting::get('conf-subm-keywords', false)) !!}
                    {!! BootForm::checkbox(trans('submissions::submissions.fields.abstract'), 'abstract')->defaultCheckedState(Setting::get('conf-subm-abstract', false)) !!}
                    {!! BootForm::checkbox(trans('submissions::submissions.fields.document'), 'document')->defaultCheckedState(Setting::get('conf-subm-document', false)) !!}

                </div>

                {!! setting_input(trans('submissions::submissions.settings.abstract_max_words'), 'conf-subm-abstract_max_words', 'text', 250)->type('number')->min(1)->step(1) !!}

            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('submissions::submissions.form.instructions') }}</h3>
            </div>
            <div class="box-body no-padding koala-box simple">
                {!! BootForm::textarea(trans('submissions::submissions.form.instructions'), 'setting[conf-subm-instructions]')->hideLabel()->value(Setting::get('conf-subm-instructions', '')) !!}
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('submissions::submissions.settings.acceptance_settings') }}</h3>
            </div>
            <div class="box-body">
                {!! BootForm::checkbox(trans('submissions::submissions.settings.show_acceptance_to_authors'), 'setting[conf-subm-accpt-show]')->defaultCheckedState(Setting::get('conf-subm-accpt-show', false)) !!}

                <div class="form-group">
                    <label class="control-label">{{ trans('submissions::submissions.settings.after_acceptance_fields') }}</label>

                    {!! BootForm::checkbox(trans('submissions::submissions.after_acceptance.final_document'), 'setting[conf-subm-accpt-final_document]')->defaultCheckedState(Setting::get('conf-subm-accpt-final_document', false)) !!}
                    {!! BootForm::checkbox(trans('submissions::submissions.after_acceptance.presenter'), 'setting[conf-subm-accpt-presenter]')->defaultCheckedState(Setting::get('conf-subm-accpt-presenter', false)) !!}


                </div>
            </div>
        </div>

        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('submissions::submissions.settings.acceptance') }}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        {!! setting_input(trans('submissions::submissions.settings.acceptance_qty'), 'conf-rev-acpt-qty', 'text', 50)->type('number')->step(1)->min(1) !!}
                    </div>
                    <div class="col-xs-5 col-sm-4 col-md-3 col-lg-2">
                        {!! BootForm::select(trans('submissions::submissions.settings.acceptance_type'), 'setting[conf-rev-acpt-type]', trans('submissions::submissions.settings.acceptance_types'))->select(old('setting.conf-rev-acpt-type', Setting::get('conf-rev-acpt-type', 'percent'))) !!}
                    </div>
                </div>
            </div>
        </div>


        {!! BootForm::submit(trans('econf.actions.update'), 'btn-primary') !!}

        {!! BootForm::close() !!}

    </section>

@endsection
