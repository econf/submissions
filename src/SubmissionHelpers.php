<?php

namespace EConf\Submissions;

use Eventy;
use Setting;
use Carbon\Carbon;

class SubmissionHelpers {

    public static $fields = [
        'name' => [
            'type' => 'text',
            'mandatory' => true,
            'validation' => 'required'
        ],
        'email' => [
            'type' => 'email',
            'mandatory' => true,
            'validation' => 'required|email'
        ],
        'address' => [
            'type' => 'textarea',
            'validation' => ''
        ],
        'country' => [
            'type' => 'country',
            'mandatory' => true,
            'validation' => 'required|country'
        ],
        'organization' => [
            'type' => 'text',
            'mandatory' => true,
            'validation' => 'required'
        ],
        'phone' => [
            'type' => 'tel',
            'validation' => 'phone:AUTO',
            'icon' => 'fa-phone'
        ],
        'website' => [
            'type' => 'url',
            'validation' => 'url'
        ],
        'facebook' => [
            'type' => 'url',
            'validation' => 'url',
            'icon' => 'fa-facebook'
        ],
        'twitter' => [
            'type' => 'url',
            'validation' => 'url',
            'icon' => 'fa-twitter'
        ],
        'google-plus' => [
            'type' => 'url',
            'validation' => 'url',
            'icon' => 'fa-google-plus'
        ],
        'instagram' => [
            'type' => 'url',
            'validation' => 'url',
            'icon' => 'fa-instagram'
        ],
        'linkedin' => [
            'type' => 'url',
            'validation' => 'url',
            'icon' => 'fa-linkedin'
        ],
        'github' => [
            'type' => 'url',
            'validation' => 'url',
            'icon' => 'fa-github'
        ],
        'biography' => [
            'type' => 'textarea',
            'validation' => ''
        ],
        'corresponding' => [
            'type' => 'checkbox',
            'mandatory' => true,
            'validation' => ''
        ]
    ];

    // Other submission functions
    public static function isOpen(){
        if(Setting::has('conf-subm-start_date') && Setting::has('conf-subm-end_date')){
            $start = Carbon::parse(Setting::get('conf-subm-start_date').' 00:00');
            $end = Carbon::parse(Setting::get('conf-subm-end_date').' 23:59:59');
            return Carbon::now()->between($start, $end);
        }
        return false;
    }

    public static function authorFieldsEach ($callback){
        $setting_fields = explode( ',', Setting::get( 'conf-subm-author_fields', '' ) );

        $fields = Eventy::filter('submissions.author-fields', SubmissionHelpers::$fields);

        foreach ( SubmissionHelpers::$fields as $n => $f ) {
            if ( !empty( $f['mandatory'] ) || in_array( $n, $setting_fields ) ) {
                call_user_func($callback, $n, $f);
            }
        }
    }

    public static function isAcceptanceOpen(){

        $default = true;

        if(Setting::has('conf-subm-end_date')){
            $end = Carbon::parse(Setting::get('conf-subm-end_date').' 23:59:59');
            $default = Carbon::now()->gt($end);
        }

        return Eventy::filter('submissions.acceptance_open', $default);
    }

    public static function shouldAskFinalDetails(){

        return Setting::get('conf-subm-accpt-show', false) && (Setting::get('conf-subm-accpt-final_document', false) || Setting::get('conf-subm-accpt-presenter', false));

    }

}
