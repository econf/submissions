<?php

namespace EConf\Submissions;

use App\Committee;
use App\SessionType;
use Carbon\Carbon;
use Date;
use EConf\Submissions\Policies\SubmissionPolicy;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;


use Eventy;
use Setting;
use Auth;
use Storage;

class SubmissionsServiceProvider extends ServiceProvider {

    /**
     * The policy mappings for the package.
     *
     * @var array
     */
    protected $policies = [
        Submission::class => SubmissionPolicy::class
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @param GateContract $gate
     * @return void
     */
    public function boot( GateContract $gate ) {

        // Load routes
        if ( !$this->app->routesAreCached() ) {
            require __DIR__ . '/Http/routes.php';
        }

        // Load breadcrumbs
        require __DIR__ . '/Http/breadcrumbs.php';

        // Load views
        $this->loadViewsFrom( __DIR__ . '/resources/views', 'submissions' );

        // Load translations
        $this->loadTranslationsFrom( __DIR__ . '/resources/lang', 'submissions' );

        // Publish migrations
        $this->publishes( [
            __DIR__ . '/database/migrations/' => database_path( 'migrations' )
        ], 'migrations' );

        $this->addPublicMenu();

        $this->addAdminMenu();

        $this->addPublicContent();

        $this->addAdminContent();

        $this->registerPolicies( $gate );

        $this->addFileLogic();

        $this->addEmails();

        Submission::deleting( function ( $submission ) {
            Storage::delete( m_path( "submissions/{$submission->id}.pdf" ) );
        } );

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register() {
        //
    }

    private function addPublicMenu() {
        // Add to public menu
        Eventy::addAction( 'public.menu.user', function ( $menu ) {
            $menu->add( trans( 'submissions::submissions.public.my' ), m_action( '\EConf\Submissions\Http\Controllers\SubmissionController@index' ) );
        }, 20, 1 );
    }

    private function addAdminMenu() {


        // Add to menu
        Eventy::addAction( 'admin.menu', function ( $menu ) {
            if ( Committee::in( 'organizing' ) ) {
                $menu->add( trans( 'submissions::submissions.menu_label' ), m_action( '\EConf\Submissions\Http\Controllers\AdminController@index' ) )
                     ->prepend( '<span class="fa fa-file-text"></span>' );
            }
            if ( Committee::in( 'program', true ) && SubmissionHelpers::isAcceptanceOpen() ) {
                $menu->add( trans( 'submissions::submissions.acceptance.label' ), m_action( '\EConf\Submissions\Http\Controllers\AdminController@acceptance' ) )
                     ->prepend( '<span class="fa fa-check-square"></span>' );
            }
        }, 20, 1 );


        // Add to settings menu
        Eventy::addAction( 'admin.menu.settings', function ( $menu ) {
            $menu->add( trans( 'submissions::submissions.settings.label_short' ), m_action( '\EConf\Submissions\Http\Controllers\SettingsController@show' ) )
                 ->prepend( '<span class="fa fa-file-text"></span>' );
        }, 20, 1 );
    }

    private function addPublicContent() {
        // Add to index
        Eventy::addAction( 'public.index', function () {
            echo view( 'submissions::public.index' );
        }, 20, 0 );
    }

    private function addAdminContent() {
        // Add to dashboard
        Eventy::addFilter( 'dashboard.data', function ( $data ) {

            $text = trans( 'submissions::submissions.status.closed' );

            if ( SubmissionHelpers::isOpen() ) {
                $text = trans( 'submissions::submissions.status.open_until', [ 'date' => Date::parse( Setting::get( 'conf-subm-end_date' ) )->format( trans( 'econf.date.dayMonth' ) ) ] );
            } else if ( Setting::has( 'conf-subm-start_date' ) && Carbon::now()->lt( Carbon::parse( Setting::get( 'conf-subm-start_date' ) ) ) ) {
                $text = trans( 'submissions::submissions.status.open_in', [ 'date' => Date::parse( Setting::get( 'conf-subm-start_date' ) )->format( trans( 'econf.date.dayMonth' ) ) ] );
            }

            $progress = 100;
            if ( SubmissionHelpers::isOpen() ) {
                // calc progress
                $psd = strtotime( Setting::get( 'conf-subm-start_date' ) . ' 00:00:00' );
                $ped = strtotime( Setting::get( 'conf-subm-end_date' ) . ' 23:59:59' );
                $ptotal = $ped - $psd;
                $pcurr = time() - $psd;
                $progress = round( ( $pcurr / $ptotal ) * 100, 2 );

            } else if ( Setting::has( 'conf-subm-start_date' ) && Carbon::now()->lt( Carbon::parse( Setting::get( 'conf-subm-start_date' ) ) ) ) {
                $progress = 0;
            }


            $data[] = [
                'label' => trans( 'submissions::submissions.dashboard_label' ),
                'value' => Submission::count(),
                'icon' => 'fa fa-file-text-o',
                'class' => 'bg-green',
                'footer' => $text,
                'progress' => $progress,
            ];
            return $data;
        }, 20, 1 );

        Eventy::addFilter( 'dashboard.calendar', function ( $calendar ) {
            if ( Setting::has( 'conf-subm-start_date' ) && Setting::has( 'conf-subm-end_date' ) ) {
                $calendar[] = [
                    'label' => trans( 'submissions::submissions.public.phase' ),
                    'start' => Date::parse( Setting::get( 'conf-subm-start_date' ) ),
                    'end' => Date::parse( Setting::get( 'conf-subm-end_date' ) ),
                ];
            }
            return $calendar;
        }, 20, 1 );
    }

    /**
     * Register the package's policies.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    protected function registerPolicies( GateContract $gate ) {
        foreach ( $this->policies as $key => $value ) {
            $gate->policy( $key, $value );
        }
    }


    /**
     * Register the logic to be added to FileController
     */
    private function addFileLogic() {

        Eventy::addFilter( 'file.can', function ( $value, $path ) {
            if ( $path[0] == 'submissions' ) {
                $last_idx = count( $path ) - 1;
                $id = explode( '.', $path[$last_idx] )[0];
                $submission = Submission::findByHiddenId( $id );
                if ( empty( $submission ) ) {
                    return false;
                }
                return $submission->user == Auth::user();
            }
            return $value;
        }, 20, 2 );

        Eventy::addFilter( 'file.transform_name', function ( $path ) {
            if ( $path[0] == 'submissions' ) {
                $last_idx = count( $path ) - 1;
                list( $hid, $ext ) = explode( '.', $path[$last_idx] );
                $path[$last_idx] = Submission::decodeHiddenId( $hid ) . '.' . $ext;
            }
            return $path;
        }, 20, 1 );

    }

    private function addEmails() {

        function emails_from_submissions( $submissions ) {
            $emails = [ ];
            foreach ( $submissions as $submission ) {
                $authors = $submission->data( 'author', [ ] );
                foreach ( $authors as $idx => $author ) {
                    if ( isset( $author['corresponding'] ) ) {
                        $a = [
                            'id' => 's' . $submission->id . 'a' . $idx,
                            'name' => $author['name'],
                            'email' => $author['email'],
                            'toString' => $author['name'] . ' - ' . $submission->title,
                            'title' => $submission->title
                        ];
                        $emails[$a['id']] = $a;
                    }
                }
            }
            return $emails;
        }

        // Add groups
        Eventy::addFilter( 'admin.email.groups', function ( $groups ) {
            $submGroups = [
                [
                    'id' => 'subm-authors-all',
                    'name' => trans( 'submissions::submissions.email.authors_all' ),
                    'emails' => function () {
                        $submissions = Submission::all();
                        return emails_from_submissions( $submissions );
                    }
                ]
            ];

            $types = SessionType::whereIn( 'id', explode( ',', Setting::get( 'conf-subm-types', '' ) ) )->get();

            if ( $types->count() > 1 ) {
                foreach ( $types as $type ) {
                    $submGroups[] = [
                        'id' => 'subm-authors-' . $type->id,
                        'name' => trans( 'submissions::submissions.email.authors_type', [ 'name' => $type->name ] ),
                        'emails' => function () use ( $type ) {
                            $submissions = Submission::where( 'session_type_id', $type->id )->get();
                            return emails_from_submissions( $submissions );
                        }
                    ];
                }
            }

            if ( SubmissionHelpers::isAcceptanceOpen() ) {
                $submGroups[] = [
                    'id' => 'submissions-accepted',
                    'name' => trans( 'reviews::reviews.email.authors_accepted' ),
                    'emails' => function () {
                        $submissions = Submission::whereAccepted( true );
                        return emails_from_submissions( $submissions );
                    }
                ];
                $submGroups[] = [
                    'id' => 'submissions-rejected',
                    'name' => trans( 'reviews::reviews.email.authors_rejected' ),
                    'emails' => function () {
                        $submissions = Submission::whereAccepted( false );
                        return emails_from_submissions( $submissions );
                    }
                ];
            }

            foreach ( $submGroups as $submGroup ) {
                $groups[$submGroup['id']] = $submGroup;
            }

            return $groups;

        }, 20, 1 );

        // Add var names
        Eventy::addFilter( 'admin.email.vars', function ( $vars ) {
            return array_merge( $vars, trans( 'submissions::submissions.email.vars' ) );
        }, 20, 1 );

    }
}
