# E-Conf Submissions

E-Conf is a Conference Management System designed to automate as much of the process as possible.

This package handles the submission phase of the conference organization. It is to be used with the [main E-Conf app][link-app-package].


![E-Conf Submissions](banner.png)

## Installation

First, require this package via Composer:

``` bash
$ composer require econf/submissions
```

After updating Composer, add the Service Provider to the providers array in `config/app.php`.

``` php
EConf\Submissions\SubmissionsServiceProvider::class
```

Finally, you need to publish the package migrations and apply them to your database:

``` bash
$ php artisan vendor:publish --provider="EConf\Submissions\SubmissionsServiceProvider"
$ php artisan migrate
```

## Usage

You can configure the submission phase through the administration panel of a conference (Organization > Settings).

## Credits

- [João Luís][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[link-app-package]: https://gitlab.com/econf/econf
[link-author]: http://joaopluis.pt
[link-eventy]: https://github.com/tormjens/eventy
